# Warning
This key will be used for authentication of raspberry device when app is started as Docker container. 

For security purposes, change it by command: ```ssh-keygen -t rsa -b 4096``` and replace pub key on raspberry pi in location ```~/.ssh/authorized_keys```.
Then rebuild image with command ```cd .. && docker build .```

**Be careful not to use your private key if you decide to push it to public Docker hub repo as it could be easily extracted from docker image layers.**
