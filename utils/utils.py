import os


def list_files(startpath):
    output = ""
    for root, dirs, files in os.walk(startpath):
        level = root.replace(startpath, '').count(os.sep)
        indent = ' ' * 4 * (level)
        output += '{}{}/'.format(indent, os.path.basename(root)) + "\n"
        subindent = ' ' * 4 * (level + 1)
        for f in files:
            output += '{}{}'.format(subindent, f) + "\n"
    return output
