FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install -y python3-dev python3-pip git ssh software-properties-common net-tools inetutils-ping

RUN apt-add-repository --yes --update ppa:ansible/ansible
RUN apt-get install -y ansible

RUN mkdir /root/.ssh
ADD ssh_keys/id_rsa /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa
ADD ssh_keys/id_rsa.pub /root/.ssh/id_rsa.pub

WORKDIR /opt
# Do not cache git clone
ADD https://www.unixtimestamp.com/ /tmp/bustcache
RUN mkdir -p ~/.ssh/
RUN ssh-keyscan -t rsa bitbucket.org > ~/.ssh/known_hosts
RUN git clone https://github.com/spodlesny/raspi_redteam bakalarka

WORKDIR /opt/bakalarka
RUN pip3 install -Ur requirements.txt
RUN ln -s /opt/bakalarka/gunicorn.service /etc/systemd/system/gunicorn.service

RUN apt-get install -y nginx
RUN rm /etc/nginx/sites-enabled/default
RUN ln -s /opt/bakalarka/proxy_config.nginx /etc/nginx/sites-enabled/

RUN git pull
RUN /usr/bin/python3 manage.py migrate
RUN /usr/bin/python3 manage.py loaddata db.json
RUN /usr/bin/python3 manage.py collectstatic --no-input

CMD service nginx start && gunicorn --access-logfile - --workers 3 --bind unix:/opt/bakalarka/bakalarka.sock Ansible_generator.wsgi:application
