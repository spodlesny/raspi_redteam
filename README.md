# RasPi Redteam
Simplyfing RaspberryPi configuration for red teams

## About
This project is developed as part of my bachelor thesis at [Brno University of Technology](https://www.vutbr.cz/en/).

# Dependencies
* Docker

## Usage
```bash
# Start web server on local port 80
docker-compose up
```
