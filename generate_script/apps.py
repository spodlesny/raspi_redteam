from django.apps import AppConfig


class GenerateScriptConfig(AppConfig):
    name = 'generate_script'
