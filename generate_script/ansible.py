import re
import tempfile
from itertools import chain

import generate_script.models as model


class Group(object):
    def add_group_header(self, group_name):
        """Create string representing a header for group (group is grouping hostnames and IPs.
        It could split playbook for roles that should be run on server side and client side for example)

        :param group_name: String representing name of group (e.g. server, clients, raspberries, etc)
        :return: string representing group header containing: host, name, remote_user and gather_facts
        """
        group = model.Group.objects.get(name=group_name)
        header_without_var_files = f"- hosts: {group.name}\n  name: {group.description}\n" \
                                   f"  remote_user: {group.remote_user}\n  become: yes\n  gather_facts: {group.gather_facts}\n"

        final_header = header_without_var_files
        list_of_vars_files = self.get_vars_files(group)
        if list_of_vars_files is not []:
            final_header += "  vars_files:\n"
            for file_location in list_of_vars_files:
                final_header += "    - {file_location}\n".format(file_location=file_location, )

        final_header += "  roles:\n"
        return final_header

    @staticmethod
    def get_vars_files(group):
        """Get list of locations of all variables files for specific group

        :param group: django DB object representing Group model
        :return: List of variables files
        """
        vars_files_list = []
        for vars_file in group.vars_files.all():
            if vars_file.location not in vars_files_list:
                vars_files_list.append(vars_file.location)

        return vars_files_list


class Utils(object):
    @staticmethod
    def create_tmp_file(file):
        """Create temporary file

        :param file: RoleFile object from models
        :return: Dictionary where key is absolute location in root directory (roles/role_name/location/filename.suffix)
        :rtype: dict
        """
        tmp_file = tempfile.NamedTemporaryFile()
        local_regexes = model.Regexp.objects.filter(role_file=file)
        global_regexes = model.Regexp.objects.filter(is_global=True)
        regexes = set(chain(local_regexes, global_regexes))
        text_data = file.text_data
        for regex in regexes:
            text_data = re.sub(regex.regex, regex.value, text_data)

        tmp_file.write(text_data.encode())
        tmp_file.flush()

        full_route = "roles/" + file.role.role_name + "/" + file.location + file.filename
        return full_route, tmp_file


class Role(object):
    def __init__(self):
        self.Group = Group()
        self.Utils = Utils()

    @staticmethod
    def get_list_of_role_objects(request):
        """Return list of role (django DB) objects from database based on django request

        :param request: Object representing django request
        :return: List of role objects
        """
        list_of_roles = request.POST.getlist("enabled")

        list_of_role_objects = []
        for role in list_of_roles:
            try:
                role = model.Role.objects.get(role_name=role)
                list_of_role_objects.append(role)
            except model.Role.DoesNotExist:
                continue

        return list_of_role_objects

    def get_list_of_role_files(self, role):
        """Get list of files

        :param role: Role name as string
        :return: List that contains dictionaries with key that is absolute location
        :rtype: list
        """
        list_of_role_files = []
        files = model.RoleFile.objects.filter(role=role)
        for file in files:
            file = self.Utils.create_tmp_file(file)
            list_of_role_files.append(file)

        return list_of_role_files


class Playbook(object):
    def __init__(self):
        self.Utils = Utils()
        self.Role = Role()

    @staticmethod
    def create_playbook_dot_yml(list_of_role_objects):
        """Create playbook.yml file

        :param list_of_role_objects: List of role objects from django DB
        :return: File named playbook.yml containing all roles (with variables) that should be started
        """
        playbook = ""
        current_group = ""
        for role in list_of_role_objects:
            for group in role.group.all():
                if current_group != group.name:
                    if current_group != "":
                        playbook += "\n"  # add new line at the end of current group
                    current_group = group.name
                    playbook += Group().add_group_header(current_group)
                playbook += "      - {role}\n".format(role=role.role_name, )

        tmp_file = tempfile.NamedTemporaryFile()
        tmp_file.write(playbook.encode())
        tmp_file.flush()

        return tmp_file

    def create_global_vars_file(self):
        tmp_file = tempfile.NamedTemporaryFile()
        var_file_content = model.GlobalVariables.objects.first()
        global_regexes = model.Regexp.objects.filter(is_global=True)

        text_data = var_file_content.text_data
        for regex in global_regexes:
            text_data = re.sub(regex.regex, regex.value, text_data)

        tmp_file.write(text_data.encode())
        tmp_file.flush()
        full_route = "vars/variables.yml"
        return full_route, tmp_file

    def generate_hosts_file(self):
        list_of_all_groups = model.Group.objects.all()
        tmp_file = tempfile.NamedTemporaryFile()

        text = ""
        for group in list_of_all_groups:
            if group.hosts.count() != 0:
                text += "[{group_name}]\n".format(group_name=group.name)
            else:
                pass
            for host in group.hosts.all():
                text += "{host}:{port}\n".format(host=host.hostname, port=host.port)
            text += "\n"
        tmp_file.write(text.encode())
        tmp_file.flush()

        full_route = "/hosts"
        return full_route, tmp_file

    def generate_playbook(self, request):
        """Generate playbook structure from POST request data

        :param request: django POST request object
        :return: Complete list of all file objects inside playbook
        :rtype: list of tuples
        """
        list_of_role_objects = self.Role.get_list_of_role_objects(request)
        list_of_files = []
        for role in list_of_role_objects:
            list_of_role_files = self.Role.get_list_of_role_files(role)
            list_of_files = list_of_files + list_of_role_files

        list_of_files = list_of_files + [("/playbook.yml", self.create_playbook_dot_yml(list_of_role_objects)),
                                         self.create_global_vars_file(), self.generate_hosts_file()]
        return list_of_files
