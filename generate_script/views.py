import os
import re
import subprocess
import zipfile
from itertools import chain
from pathlib import Path

from django.db import IntegrityError
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import render

# Create your views here.
from django.urls import reverse, resolve
from django.views.generic import FormView, DetailView, TemplateView, ListView

from Ansible_generator.settings import local_run_storage
from generate_script.ansible import Playbook
from generate_script.utils import generate_zip, check_requirements
from generate_script.forms import GenerateScript
from generate_script.models import Role, RoleFile, Regexp, GlobalVariables, Group, Host


class Index(ListView):
    template_name = 'generate_script/configurator.html'
    model = Role
    storage_location = local_run_storage + "playbook"
    log_location = storage_location + "_log.txt"
    err_log_location = storage_location + "_stderr_log.txt"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["ordered_roles"] = Role.objects.all().order_by("build_order")
        if resolve(self.request.path_info).url_name == "run_command":
            context["data"] = Path(self.log_location).read_text()
            if not context["data"]:
                context["data"] = "a"
        return context

    def post(self, request, *args, **kwargs):
        # ToDo: refactor
        if request.POST.get("build"):
            counter = 0
            for role in request.POST.getlist("order_position"):
                db_role = Role.objects.get(role_name=role)
                if db_role.build_order != counter:
                    db_role.build_order = counter
                    db_role.save()
                counter += 1

            for role in request.POST.getlist("enabled"):
                db_role = Role.objects.get(role_name=role)
                db_role.enabled = True
                db_role.save()

            for db_role in Role.objects.all():
                if db_role.role_name not in request.POST.getlist("enabled"):
                    print(db_role.role_name)
                    db_role.enabled = False
                    db_role.save()

            playbook = Playbook().generate_playbook(request)
            file_path = generate_zip(playbook)

            #  source: https://stackoverflow.com/a/36394206
            if os.path.exists(file_path):
                with open(file_path, 'rb') as fh:
                    response = HttpResponse(fh.read(), content_type="application/zip")
                    response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
                    return response
            raise Http404

        if request.POST.get("run"):
            counter = 0
            for role in request.POST.getlist("order_position"):
                db_role = Role.objects.get(role_name=role)
                if db_role.build_order != counter:
                    db_role.build_order = counter
                    db_role.save()
                counter += 1

            for role in request.POST.getlist("enabled"):
                db_role = Role.objects.get(role_name=role)
                db_role.enabled = True
                db_role.save()

            for db_role in Role.objects.all():
                if db_role.role_name not in request.POST.getlist("enabled"):
                    db_role.enabled = False
                    db_role.save()

            playbook = Playbook().generate_playbook(request)
            file_path = generate_zip(playbook)

            if check_requirements() and os.path.exists(file_path):
                zip_ref = zipfile.ZipFile(file_path, 'r')
                zip_ref.extractall(self.storage_location)
                zip_ref.close()

                log_file = open(self.log_location, "wb")
                err_log_file = open(self.err_log_location, "wb")

                try:
                    subprocess.Popen(["ansible-playbook", "-i", "hosts", "playbook.yml"], cwd=self.storage_location, stderr=err_log_file, stdout=log_file)
                except subprocess.CalledProcessError as error:
                    log_file.write(error.output)

                log_file.flush()

                return HttpResponseRedirect(reverse('generate_src:run_command'))
            else:
                raise Http404


class ApplyRegex(DetailView):
    template_name = 'generate_script/detail_config_option.html'
    model = Role
    pk_url_kwarg = "role"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        role = Role.objects.get(role_name=self.kwargs["role"])
        context["files"] = RoleFile.objects.filter(role=role)
        context["is_final"] = True

        if not "file_with_location" in self.kwargs:
            self.kwargs["file_with_location"] = "tasks/main.yml"

        file_name = self.kwargs["file_with_location"].split("/")[-1]
        file_location = "/".join(self.kwargs["file_with_location"].split("/")[:-1]) + "/"
        local_regexps = Regexp.objects.filter(role_file=RoleFile.objects.get(role=role,
                                                                             filename=file_name,
                                                                             location=file_location))
        global_regexps = Regexp.objects.filter(is_global=True)
        context["regexps"] = set(chain(local_regexps, global_regexps))
        file = RoleFile.objects.get(role=role, filename=file_name, location=file_location)

        for x in context["regexps"]:
            file.text_data = re.sub(x.regex, x.value, file.text_data)
            print(file.text_data)

        context["file"] = file
        return context


class ConfigureRegex(DetailView):
    template_name = 'generate_script/detail_config_option.html'
    model = Role
    pk_url_kwarg = "role"

    def post(self, request, *args, **kwargs):
        # ToDo: allow changing also a name of regex (or at least show it as error)
        role = request.POST.get("role")
        role_file = RoleFile.objects.get(filename=request.POST.get("filename"), role=role)
        regex, created = Regexp.objects.get_or_create(name=request.POST.get("name"), role_file=role_file, is_global=bool(request.POST.get("is_global"), ))
        if created:
            try:
                original_regex = Regexp.objects.get(name=request.POST.get("name"), is_global=not bool(request.POST.get("is_global"), ))
                original_regex.delete(keep_parents=True)
            except Regexp.DoesNotExist:
                pass
        regex.value = request.POST.get("input")
        regex.regex = request.POST.get("regex")
        regex.save()

        return HttpResponseRedirect(reverse('generate_src:detail_config', kwargs={'pk': request.POST.get("role"),
                                                                                  "slug": request.POST.get(
                                                                                      "location") + request.POST.get(
                                                                                      "filename")}))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        role = Role.objects.get(role_name=self.kwargs["role"])
        file = RoleFile.objects.get(role=role, filename=self.kwargs["file_name"])
        try:
            context["regex"] = Regexp.objects.get(role_file=file, name=self.kwargs["regex_name"])
        except Regexp.DoesNotExist:
            context["regex"] = Regexp.objects.get(name=self.kwargs["regex_name"], is_global=True)

        context["files"] = RoleFile.objects.filter(role=role)
        local_regexps = Regexp.objects.filter(role_file=RoleFile.objects.get(role=role,
                                                                             filename=file.filename,
                                                                             location=file.location))
        global_regexps = Regexp.objects.filter(is_global=True)
        context["regexps"] = set(chain(local_regexps, global_regexps))
        context["file"] = RoleFile.objects.get(role=role,
                                               filename=file.filename,
                                               location=file.location)
        return context


class ConfigureRole(DetailView):
    template_name = 'generate_script/detail_config_option.html'
    model = Role

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        role = Role.objects.get(role_name=self.kwargs["pk"])
        context["files"] = RoleFile.objects.filter(role=role)

        if not "slug" in self.kwargs:
            self.kwargs["slug"] = "tasks/main.yml"

        file_name = self.kwargs["slug"].split("/")[-1]
        file_location = "/".join(self.kwargs["slug"].split("/")[:-1]) + "/"
        local_regexps = Regexp.objects.filter(role_file=RoleFile.objects.get(role=role,
                                                                             filename=file_name,
                                                                             location=file_location))
        global_regexps = Regexp.objects.filter(is_global=True)
        context["regexps"] = set(chain(local_regexps, global_regexps))
        context["file"] = RoleFile.objects.get(role=role,
                                               filename=file_name,
                                               location=file_location)
        return context


class ConfigureGlobalVariables(ListView):
    template_name = 'generate_script/detail_config_global_variables.html'
    model = GlobalVariables

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["file"] = GlobalVariables.objects.first()
        context["regexps"] = Regexp.objects.filter(is_global=True)
        return context

class ConfigureHosts(TemplateView):
    template_name = 'generate_script/manage_groups.html'

    def post(self, request, *args, **kwargs):
        if self.kwargs.get("action"):
            group = Group.objects.get(name=self.kwargs.get("group_name"))
            if self.kwargs.get("action") == "delete_host":
                host = Host.objects.get(hostname=self.kwargs.get("host_name"))
                group.hosts.remove(host)
            if self.kwargs.get("action") == "add_new_host":
                host,_ = Host.objects.get_or_create(hostname=self.request.POST.get("hostname"))
                group.hosts.add(host)
            return HttpResponseRedirect(reverse("generate_src:configure_hosts", kwargs={"group_name":self.kwargs.get("group_name")}))
        else:
            return Http404

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["groups"] = Group.objects.all()
        if not self.kwargs.get("group_name"):
            context["group"] = Group.objects.first()
        else:
            context["group"] = Group.objects.get(name=self.kwargs.get("group_name"))

        context["hosts"] = Host.objects.filter(group=context["group"])
        return context


def get_output(request):
    if request.method == "GET":
        if Path(Index().log_location).read_text():
            return HttpResponse(Path(Index().log_location).read_text(), content_type="text/html")
        elif Path(Index().err_log_location).read_text():
            return HttpResponse(Path(Index().err_log_location).read_text(), content_type="text/html")
        else:
            return HttpResponse("No output yet", content_type="text/html")
    else:
        return HttpResponse(status=405)
