# Generated by Django 2.0.4 on 2018-05-16 06:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('generate_script', '0004_auto_20180510_0754'),
    ]

    operations = [
        migrations.RenameField(
            model_name='rolefile',
            old_name='playbook',
            new_name='role',
        ),
    ]
