# Generated by Django 2.1.2 on 2018-10-23 13:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('generate_script', '0044_auto_20181023_1223'),
    ]

    operations = [
        migrations.AlterField(
            model_name='regexp',
            name='role_file',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='generate_script.RoleFile'),
        ),
    ]
