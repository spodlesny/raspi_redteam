# Generated by Django 2.0.4 on 2018-06-26 07:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('generate_script', '0023_auto_20180626_0710'),
    ]

    operations = [
        migrations.AddField(
            model_name='role',
            name='enabled',
            field=models.BooleanField(default=False),
        ),
    ]
