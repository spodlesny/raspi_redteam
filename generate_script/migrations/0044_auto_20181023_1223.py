# Generated by Django 2.1.2 on 2018-10-23 12:23

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('generate_script', '0043_auto_20181017_2005'),
    ]

    operations = [
        migrations.AlterField(
            model_name='regexp',
            name='role_file',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='generate_script.RoleFile'),
        ),
    ]
