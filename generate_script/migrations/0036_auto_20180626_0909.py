# Generated by Django 2.0.4 on 2018-06-26 09:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('generate_script', '0035_group_description'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='remote_user',
            field=models.CharField(default='root', max_length=256),
        ),
    ]
