# Generated by Django 2.0.4 on 2018-04-29 13:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Playbook',
            fields=[
                ('role_name', models.CharField(max_length=128, primary_key=True, serialize=False)),
            ],
        ),
        migrations.CreateModel(
            name='Regex',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('regex', models.CharField(max_length=512)),
                ('value', models.CharField(max_length=512)),
            ],
        ),
        migrations.CreateModel(
            name='RoleFile',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('template', models.BooleanField(default=False)),
                ('filename', models.CharField(max_length=256)),
                ('text_data', models.TextField(blank=True, null=True)),
                ('binary_data', models.BinaryField(blank=True, null=True)),
                ('playbook', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='generate_script.Playbook')),
            ],
        ),
        migrations.CreateModel(
            name='Value',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=512)),
                ('value', models.CharField(max_length=512)),
                ('role', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='generate_script.RoleFile')),
            ],
        ),
        migrations.AddField(
            model_name='regex',
            name='role',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='generate_script.RoleFile'),
        ),
    ]
