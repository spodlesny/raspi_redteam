from django.contrib import admin
from generate_script.models import *

# Register your models here.


admin.site.register(Regexp)
admin.site.register(Value)
admin.site.register(Category)
admin.site.register(Group)
admin.site.register(Host)
admin.site.register(VarsFile)
admin.site.register(RoleFile)
admin.site.register(GlobalVariables)

# ToDo: https://stackoverflow.com/questions/26309431/django-admin-can-i-define-fields-order
class RoleFileInline(admin.TabularInline):
    extra = 0
    model = RoleFile

@admin.register(Role)
class RoleSetup(admin.ModelAdmin):
    inlines = [
        RoleFileInline,
    ]
