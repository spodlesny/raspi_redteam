import os
import zipfile
from random import choice
from string import ascii_letters

from Ansible_generator import settings

def random_string(length = 12):
    return ''.join(choice(ascii_letters) for i in range(length))

def generate_zip(playbook):
    """Generate zip file from files stored in list

    :param playbook: List of tuples where first param is location (str) and second is fileobject of file.
    :rtype: Absolute file path to file  # ToDo: mozna zranitelnost?
    """
    random_directory = random_string(12)
    os.makedirs(os.path.join(settings.STATIC_ROOT, "playbooks"+"/"+random_directory+"/"))
    file_path = os.path.join(settings.STATIC_ROOT, "playbooks"+"/"+random_directory+"/"+"playbook.zip")
    zip_file = zipfile.ZipFile(file_path, "w")
    for location, file_obj in playbook:
        zip_file.write(filename=file_obj.name, arcname=location)

    zip_file.close()
    return file_path

def check_requirements():
    #ToDo dokoncit
    return True
