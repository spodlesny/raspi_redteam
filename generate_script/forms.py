from django import forms

device_access = (
    ('password_change', 'Change password'),
    ('hostname_change', 'Change hostname'),
)

ethernet_attacks = (
    ('mitmf', "MiTM Framework"),
    ('bettercap', "Bettercap"),
    ('thc_ipv6', "THC IPv6 Attack toolkit"),
)

wifi_attacks = (
    ("wifi_connection", "Simple WiFi connection"),
    ("pumpkin_wifi", "Pumpkin wifi"),
    ("aircrack", "Aircrack-ng"),
)

class GenerateScript(forms.Form):
    device_access = forms.MultipleChoiceField(
        required=False,
        widget=forms.CheckboxSelectMultiple,
        choices=device_access,
    )

    ethernet_attack_tools = forms.MultipleChoiceField(
        required=False,
        widget=forms.CheckboxSelectMultiple,
        choices=ethernet_attacks,
    )

    wifi_attack_tools = forms.MultipleChoiceField(
        required=False,
        widget=forms.CheckboxSelectMultiple,
        choices=wifi_attacks,
    )
