from django.conf.urls import url

from generate_script.views import Index, ConfigureRole, ConfigureRegex, ApplyRegex, ConfigureGlobalVariables, \
    ConfigureHosts, get_output

app_name = "generate_src"

urlpatterns = [
    url(r'^$', Index.as_view(), name='index'),
    url(r'run/$', Index.as_view(), name='run_command'),
    url(r'regex/(?P<role>[-\w]+)/(?P<file_name>[-.\w]+)/(?P<regex_name>[-\s\w]+)$', ConfigureRegex.as_view(),
        name='regex'),
    url(r'apply_regex/(?P<role>[-\w]+)/(?P<file_with_location>[-_/.\w]+)$', ApplyRegex.as_view(), name='apply_regex'),
    url(r'save_regex/$', ConfigureRegex.as_view(), name='save_regex'),
    url(r'config/(?P<pk>[-\w]+)/$', ConfigureRole.as_view(), name='config'),
    url(r'config/(?P<pk>[-\w]+)/(?P<slug>[-_/.\w]+)$', ConfigureRole.as_view(), name='detail_config'),
    url(r'config/global_variables$', ConfigureGlobalVariables.as_view(), name='configure_global_variables'),
    url(r'hosts$', ConfigureHosts.as_view(), name="configure_hosts"),
    url(r'hosts/(?P<group_name>[-\s\w]+)$', ConfigureHosts.as_view(), name="configure_hosts"),
    url(r'hosts/(?P<group_name>[-\s\w.]+)/(?P<host_name>[-\s\w.]+)/(?P<action>[-\s\w]+)', ConfigureHosts.as_view(), name="perform_action"),
    url(r'hosts/(?P<group_name>[-\s\w.]+)/(?P<action>[-\s\w.]+)', ConfigureHosts.as_view(), name="perform_action"),
    url(r"output/$", get_output)
]
