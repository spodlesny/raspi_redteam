from generate_script.models import Category, Role


def menu_processor(request):
    return {'menu_items': Category.objects.all().order_by('position'),
            'roles': Role.objects.all()}