from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

# Create your models here.
class VarsFile(models.Model):
    location = models.CharField(primary_key=True, default="vars/variables.yml", max_length=128)

# ToDo dokumentation: mention that only first variable file is used
class GlobalVariables(models.Model):
    language = models.CharField(max_length=16,
                                choices=(("yaml", "YAML"),
                                         ("none", "Plaintext")),
                                default="yaml")
    text_data = models.TextField(primary_key=True)

class Host(models.Model):
    hostname = models.CharField(max_length=256, primary_key=True)
    port = models.IntegerField(default=22, validators=[MaxValueValidator(65535), MinValueValidator(1)])


class Group(models.Model):
    name = models.CharField(max_length=128, primary_key=True)
    description = models.CharField(max_length=256, blank=True, null=True)
    remote_user = models.CharField(max_length=256, default="root")
    vars_files = models.ManyToManyField(VarsFile, default="vars/variables.yml")
    gather_facts = models.BooleanField(default=True)
    hosts = models.ManyToManyField(Host)

class Category(models.Model):
    position = models.IntegerField(default=9)
    name = models.CharField(max_length=32, primary_key=True, default="General")

    class Meta:
        verbose_name_plural = "Categories"

class Role(models.Model):
    role_description = models.CharField(max_length=32)
    role_name = models.CharField(max_length=128, primary_key=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, blank=True, null=True, default="General")
    group = models.ManyToManyField(Group)
    build_order = models.IntegerField(default= 99, validators=[MinValueValidator(0),]) # ToDo dokumentacia: spomenut ze chovanie je undefined ak je rovnaky build order
    enabled = models.BooleanField(default=True)

    def __str__(self):
        return self.role_description


class RoleFile(models.Model):
    role = models.ForeignKey(Role, on_delete=models.CASCADE)
    template = models.BooleanField(default=False, blank=True)
    filename = models.CharField(max_length=256)
    language = models.CharField(max_length=16,
                                choices=(("yaml", "YAML"),
                                         ("none", "Plaintext")),
                                default="yaml")
    location = models.CharField(max_length=512, default="tasks/")
    text_data = models.TextField(null=True, blank=True)
    binary_data = models.BinaryField(null=True, blank=True)

    def __str__(self):
        return "{}/{}{}".format(self.role.role_name,self.location, self.filename)


class Regexp(models.Model):
    role_file = models.ForeignKey(RoleFile, on_delete=models.CASCADE)
    name = models.CharField(max_length=64)
    regex = models.CharField(max_length=512)
    value = models.CharField(max_length=512, blank=True, null=True)
    is_global = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = (("name", "is_global"),)


class Value(models.Model):
    id = models.AutoField(primary_key=True)
    role = models.ForeignKey(RoleFile, on_delete=models.CASCADE)
    name = models.CharField(max_length=512)
    value = models.CharField(max_length=512, blank=True, null=True)

    def __str__(self):
        return self.name
